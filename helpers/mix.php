<?php

use KDA\Laravel\PackageMix\Facades\AssetManager;

if(!function_exists('vendor_mix')){
    function vendor_mix($vendor,$path,$force_bust=false){
        return AssetManager::mix($vendor,$path);
    }
}

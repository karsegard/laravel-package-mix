<?php

namespace KDA\Laravel\PackageMix\Http\Controllers;

use Illuminate\Support\Str;
use KDA\Laravel\Layouts\Facades\LayoutManager;
use KDA\Laravel\PackageMix\Facades\AssetManager;
use KDA\Laravel\PackageMix\Http\Controllers\Concerns\CanPretendToBeAFile;

class AssetController
{
    use CanPretendToBeAFile;
    public function __invoke(string $vendor,string $file)
    {
        $types = AssetManager::getTypes();
        $path      = parse_url($file, PHP_URL_PATH);       // get path from url
        $extension = pathinfo($path, PATHINFO_EXTENSION); // get ext from path
        if(isset($types[$extension])){

            $file = AssetManager::getPackageAssetPath($vendor,$file);
            if(!file_exists($file)){
                abort(404);
            }
            return $this->pretendResponseIsFile($file,$types[$extension]);
        }
      /*  if (Str::endsWith($file, '.js')) {
            $name = $file;
            $path = LayoutManager::findScriptPath($name);
            abort_unless(
                !blank($path),
                404,
            );
            return $this->pretendResponseIsFile($path, 'application/javascript; charset=utf-8');
        }
        if (Str::endsWith($file, '.css')) {
            $name = $file;
            $path = LayoutManager::findStylePath($name);
            abort_unless(
                !blank($path),
                404,
            );
            return $this->pretendResponseIsFile($path, 'text/css; charset=utf-8');
        }*/
        abort(404);
    }
}

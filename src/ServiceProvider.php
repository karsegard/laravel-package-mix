<?php
namespace KDA\Laravel\PackageMix;

use Illuminate\Support\Facades\Route;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
use KDA\Laravel\Traits\HasHelper;
//use Illuminate\Support\Facades\Blade;
use KDA\Laravel\PackageMix\Facades\AssetManager as Facade;
use KDA\Laravel\PackageMix\AssetManager as Library;
use KDA\Laravel\PackageMix\Http\Controllers\AssetController;

class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasHelper;
    protected $packageName ='laravel-package-mix';
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    public function register()
    {
        parent::register();
        $this->app->singleton(Facade::class, function () {
            return new Library();
        });
    }
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){

        Route::get('/mix_debug/{vendor}/{path}', AssetController::class)->where('path', '.*')->name('mix_debug');

    }
}

<?php

namespace KDA\Laravel\PackageMix;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

//use Illuminate\Support\Facades\Blade;
class AssetManager
{
    protected Collection $vendors;
    protected array $types = ['js' => 'application/javascript; charset=utf-8', 'css' => 'text/css; charset=utf-8','gif'=>'image/gif'
    ,'jpg'=>'image/jpeg','png'=>'image/png'];

    public function __construct()
    {
        $this->vendors = collect([]);
    }

    public function getPublicAssetPath($vendor, $path)
    {
        return public_path($this->getAssetPath($vendor,$path));
    }

    public function getAssetPath($vendor, $path)
    {
        return '/vendor' . str($vendor)->start('/') . str($path)->start('/');
    }

    public function getPackageAssetPath($vendor,$path):?string
    {
        $v = $this->vendors->get($vendor);
        if($v){
            return str($v['path'])->finish('/') . str($path)->ltrim('/');
        }

        return null;
    }

    public function registerVendor($vendor, $path, $manifest = 'mix-manifest.json')
    {
        $this->vendors->put($vendor, ['path' => $path, 'manifest' => $manifest]);
    }

    public function types(array $types): static
    {
        $this->types = $types;
        return $this;
    }

    public function getTypes(): array
    {
        return $this->types;
    }


    public function getDebugManifestPath($vendor):?string
    {
        $vendor = $this->vendors->get($vendor);
        if ($vendor) {
            return $vendor['path'] . "/" . $vendor['manifest'];
        }
        return null;
    }
    
    public function getManifestPath($vendor):?string
    {
        $_vendor = $this->vendors->get($vendor);
        if ($_vendor) {
            return $this->getPublicAssetPath($vendor,$_vendor['manifest']);
        }
        return null;
    }


    public function getManifestId($vendor, $path)
    {
        $id = null;
        $manifest = $this->getManifestPath($vendor);
        if (file_exists($manifest)) {
            $manifest = json_decode(file_get_contents($manifest), true);
            if (Arr::get($manifest, $path)) {
                $url = parse_url(Arr::get($manifest, $path), PHP_URL_QUERY);
                if ($url) {
                    list($field, $id) = explode('=', $url);
                }
            }
        }
        return $id;
    }

    public function getDebugManifestId($vendor, $path)
    {
        $id = null;
        $manifest = $this->getDebugManifestPath($vendor);
        if (file_exists($manifest)) {
            $manifest = json_decode(file_get_contents($manifest), true);
            if (Arr::get($manifest, $path)) {
                $url = parse_url(Arr::get($manifest, $path), PHP_URL_QUERY);
                if ($url) {
                    list($field, $id) = explode('=', $url);
                }
            }
        }
        return $id;
    }

    public function getDebugRoute(){
        return 'mix_debug';
    }

    public function mix($vendor,$path,$bust=false): string
    {
        $debug = config('app.debug');
        if ($debug) {
            $id = $this->getDebugManifestId($vendor,$path);
            if(!$id){
                $id = \Str::random(10);
            }
            return route($this->getDebugRoute(), ['path' => str($path)->ltrim('/'), 'vendor' => $vendor, 'id' => $id]) ;
         //   return "<script defer src=\"" . route(LayoutManager::getScriptDebugRoute(), ['file' => "{$name}", 'vendor' => $vendor, 'id' => $id]) . "\" ></script>";
        } else {
            
            if (!file_exists($this->getManifestPath($vendor))) {
                throw new \Exception("Vendor ".$vendor." asset not published, file: ".$path.' manifest:'.$this->getManifestPath($vendor));
               // $html = "<script>console.warn('some files aren\'t published for vendor " . $vendor . "')</script>";
                //    dump('not published');
             /*   $id = $this->getDebugManifestId($path);
                $html .= "<script defer src=\"" . route(LayoutManager::getScriptDebugRoute(), ['file' => "{$name}", 'vendor' => $vendor, 'id' => $id]) . "\" ></script>";
                return $html;*/
            }else{
                $id = $this->getManifestId($vendor,$path);
                return $this->getAssetPath($vendor,$path).(!blank($id) ? '?'.$id:'');
            }
        }
    }
}

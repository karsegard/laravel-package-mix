<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;
use KDA\Laravel\PackageMix\Facades\AssetManager;
use KDA\Tests\TestCase;

class ModelTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function debug_manifest()
  {
    AssetManager::registerVendor('test-vendor',__DIR__.'/../testpackage');

    $this->assertTrue(file_exists(AssetManager::getDebugManifestPath('test-vendor')));

    $id = AssetManager::getDebugManifestId('test-vendor','/assets/js/app.js');
    $this->assertNotEmpty($id);

    $id = AssetManager::getDebugManifestId('test-vendor','/assets/css/app.css');

    $this->assertEmpty($id);
  
  
  }


  /** @test */
  function published_manifest()
  {
    AssetManager::registerVendor('test-vendor',__DIR__.'/../testpackage');
    $manifest = AssetManager::getManifestPath('test-vendor');
    $this->assertTrue(file_exists($manifest));

    $public = AssetManager::getPublicAssetPath('test-vendor','/assets/js/app.js');
    $this->assertTrue(file_exists($public));
  }

  /** @test */
  function inexistant_debug_manifest_path()
  {
    AssetManager::registerVendor('test-vendor',__DIR__.'/../testpackage');

    $this->assertEmpty(AssetManager::getDebugManifestPath('test-vendorafsd'));

  }


  /** @test */
  function inexistant_manifest_path()
  {
    AssetManager::registerVendor('test-vendor',__DIR__.'/../testpackage');

    $this->assertEmpty(AssetManager::getManifestPath('test-vendorafsd'));

  }


  /** @test */
  function unpublished_production_mix_url()
  {
    Config::set('app.debug',false);
    $this->expectException(\Exception::class);
    AssetManager::registerVendor('test-vendor2',__DIR__.'/../testpackagehello');
    
    AssetManager::mix('test-vendor','/assets/js/app.js');
   
  }
  


  /** @test */
  function unpublished_debug_mix_url()
  {
    Config::set('app.debug',true);
    AssetManager::registerVendor('test-vendor',__DIR__.'/../testpackage');
    
    $file = AssetManager::mix('test-vendor','/assets/js/app.js');
    $response = $this->get($file);
    $response->assertStatus(200);
  }
}